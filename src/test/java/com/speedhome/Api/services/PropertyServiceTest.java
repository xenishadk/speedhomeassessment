package com.speedhome.Api.services;

import com.speedhome.Api.models.Property;
import com.speedhome.Api.repositories.PropertyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.mockito.Mockito.any;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class PropertyServiceTest {

    @MockBean
    private PropertyRepository propertyRepository;

    @Autowired
    private PropertyService propertyService;

    @Test
    void testGetAllProperties_whenPropertiesAreNotAvailable(){
        Mockito.when(propertyRepository.findAll()).thenReturn(new ArrayList<>());
        List<Property> properties = propertyService.getAllProperties();
        Assertions.assertEquals(properties.size(), 0);
    }

    @Test
    void testGetAllProperties(){
        Property propertyCyberJaya = new Property(
                "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
                2, 1, 0, "sample description"
        );
        Property propertyKL = new Property(
                "Mr Y property", "Kuala Lumpur, Ampang Road", 1200, 2000,
                4, 2, 1, "sample description for KL"
        );
        Mockito.when(propertyRepository.findAll()).thenReturn(Arrays.asList(propertyCyberJaya, propertyKL));
        List<Property> properties = propertyService.getAllProperties();
        Assertions.assertEquals(properties.size(), 2);
    }

    @Test
    void testGetPropertyById_whenNoPropertyIsFound(){
        Mockito.when(propertyRepository.findById(any(Long.class))).thenReturn(null);
        Optional<Property> property = propertyService.getPropertyById(any(Long.class));
        Assertions.assertEquals(property, null);
    }

    @Test
    void testGetPropertyById(){
        Property propertyCyberJaya = new Property(
          "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
          2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Mockito.when(propertyRepository.findById(propertyCyberJaya.getId())).thenReturn(Optional.of(propertyCyberJaya));
        Optional<Property> property = propertyService.getPropertyById(1L);
        Assertions.assertNotEquals(property, null);
        Assertions.assertEquals(property.get().getId(), propertyCyberJaya.getId());
    }

    @Test
    void testAddNewProperty(){
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        Mockito.when(propertyRepository.save(any(Property.class))).thenReturn(propertyCyberJaya);
        Property savedProperty = propertyService.addNewProperty(propertyCyberJaya);
        Assertions.assertEquals(savedProperty.getName(), propertyCyberJaya.getName());
    }

    @Test
    void updateProperty(){
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Property updatedPropertyCyberJaya = new Property(
                "Mr X property updared", "cyberjaya, Teknokrat 2, Jalan", 1200, 1500,
                2, 1, 0, "sample description"
        );
        updatedPropertyCyberJaya.setId(1L);
        Mockito.when(propertyRepository.findById(propertyCyberJaya.getId())).thenReturn(Optional.of(propertyCyberJaya));
        Mockito.when(propertyRepository.save(propertyCyberJaya)).thenReturn(updatedPropertyCyberJaya);
        Property updatedProperty = propertyService.updateProperty(propertyCyberJaya.getId(), propertyCyberJaya);
        Assertions.assertNotEquals(propertyCyberJaya.getArea(), updatedProperty.getArea());
        Assertions.assertNotEquals(propertyCyberJaya.getPrice(), updatedProperty.getPrice());
        Assertions.assertNotEquals(propertyCyberJaya.getName(), updatedProperty.getName());
        Assertions.assertEquals(propertyCyberJaya.getId(), updatedProperty.getId());
    }

    @Test
    void testUpdateProperty_whenPropertyIsInvalid(){
        Property propertyCyberJaya = new Property(
                "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
                2, 1, 0, "sample description"
        );
        Mockito.when(propertyRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        Property updatedProperty = propertyService.updateProperty(1L, propertyCyberJaya);
        Assertions.assertEquals(updatedProperty, null);
    }

    @Test
    void testSearchProperty(){
        Property propertyCyberJaya = new Property(
                "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
                2, 1, 0, "sample description"
        );
        String name = "X";
        Integer area = 900;
        Integer price = 1000;
        Mockito.when(propertyRepository.searchProperty(name, area, price)).thenReturn(Arrays.asList(propertyCyberJaya));
        List<Property> searchResult = propertyService.searchProperties(name, area, price);
        Assertions.assertEquals(searchResult.size(), 1);
    }

    @Test
    void testApproveProperty(){
        Property propertyCyberJaya = new Property(
            "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
            2, 1, 0, "sample description"
        );
        Mockito.when(propertyRepository.findById(1L)).thenReturn(Optional.of(propertyCyberJaya));
        Property property = propertyService.approveProperty(1L);
        Assertions.assertEquals(property.isApproved(), true);

    }

}
