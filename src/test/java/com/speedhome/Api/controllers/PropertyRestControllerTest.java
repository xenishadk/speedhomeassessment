package com.speedhome.Api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.speedhome.Api.models.Property;
import com.speedhome.Api.services.PropertyService;
import static org.mockito.Mockito.any;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class PropertyRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PropertyService propertyService;

    @Test
    void testPropertiesApiWithoutToken_shouldReturnUnauthorized() throws Exception{
        mockMvc.perform(get("/properties"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(get("/properties/1"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(post("/properties"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(put("/properties/1"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(post("/properties/search"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testGetAllProperties_whenPropertiesAreNotAvailable() throws Exception{
        Mockito.when(propertyService.getAllProperties()).thenReturn(new ArrayList<>());
        mockMvc.perform(get("/properties")
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    void testGetAllProperties() throws Exception{
        Property propertyCyberJaya = new Property(
            "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
            2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Property propertyKL = new Property(
            "Mr Y property", "Kuala Lumpur, Ampang Road", 1200, 2000,
            4, 2, 1, "sample description for KL"
        );
        propertyKL.setId(2L);
        Mockito.when(propertyService.getAllProperties()).thenReturn(Arrays.asList(propertyCyberJaya, propertyKL));
        mockMvc.perform(get("/properties")
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].id").value(containsInAnyOrder(1,2)));
    }

    @Test
    void testGetProperty_whenPropertyDoesNotExist() throws Exception{
        Mockito.when(propertyService.getPropertyById(any(Long.class))).thenReturn(Optional.empty());
        mockMvc.perform(get("/properties/{id}", 1L)
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.details").value("Property Not Found"));
    }

    @Test
    void testGetProperty() throws Exception{
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Mockito.when(propertyService.getPropertyById(1L)).thenReturn(Optional.of(propertyCyberJaya));
        mockMvc.perform(get("/properties/{id}", 1L)
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(propertyCyberJaya.getId()));
    }

    @Test
    void testSaveProperty() throws Exception{
        Property propertyCyberJaya = new Property(
            "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
            2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Mockito.when(propertyService.addNewProperty(any(Property.class))).thenReturn(propertyCyberJaya);
        mockMvc.perform(post("/properties")
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonData(propertyCyberJaya)))
                .andExpect(status().isCreated())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void testUpdateProperty_whenPropertyDoesNotExist() throws Exception{
        Property propertyCyberJaya = new Property(
                "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
                2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Mockito.when(propertyService.updateProperty(any(Long.class), any(Property.class))).thenReturn(null);
        mockMvc.perform(put("/properties/{id}", 1)
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonData(propertyCyberJaya)))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.details").value("Property Not Found"));
    }

    @Test
    void testUpdateProperty() throws Exception{
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        Mockito.when(propertyService.updateProperty(any(Long.class), any(Property.class))).thenReturn(propertyCyberJaya);
        mockMvc.perform(put("/properties/{id}", 1)
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonData(propertyCyberJaya)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void testSearchProperty() throws Exception{
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        propertyCyberJaya.setId(1L);
        HashMap<String, ?> searchQuery = new HashMap<>(){{
            put("address", "cyberjaya");
            put("area", 500);
            put("price", 1000);
        }};
        Mockito.when(propertyService.searchProperties("cyberjaya", 500, 1000)).thenReturn(Arrays.asList(propertyCyberJaya));
        mockMvc.perform(post("/properties/search")
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonData(searchQuery)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].area").value(greaterThanOrEqualTo(propertyCyberJaya.getArea())))
                .andExpect(jsonPath("$[0].price").value(greaterThanOrEqualTo(propertyCyberJaya.getPrice())))
                .andExpect(jsonPath("$[0].address").value(containsString(propertyCyberJaya.getAddress())));
    }

    @Test
    void testApproveProperty() throws Exception{
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        propertyCyberJaya.setApproved(true);
        Mockito.when(propertyService.approveProperty(1L)).thenReturn(propertyCyberJaya);
        mockMvc.perform(get("/properties/approve/{id}", 1L)
                .header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.details").value("Property Approved Successfully"));
    }

    private static String asJsonData(Object obj){
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
