package com.speedhome.Api.repositories;

import com.speedhome.Api.models.Property;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Description;

import java.util.Arrays;
import java.util.List;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class PropertyRepositoryTests {

    @Autowired
    private PropertyRepository propertyRepository;

    @Test
    void testSearchProperties(){
        Property propertyCyberJaya = new Property(
           "Mr X property", "cyberjaya, Teknokrat 2, Jalan", 900, 1200,
           2, 1, 0, "sample description"
        );
        Property propertyKL = new Property(
           "Mr Y property", "Kuala Lumpur, Ampang Road", 1200, 2000,
           4, 2, 1, "sample description for KL"
        );
        Property propertyKlang = new Property(
                "Mr Y property", "Klang and Kuala Lumpur, cyberjaya, Ampang Road", 1200, 2000,
                1, 1, 1, "description for Klang"
        );
        propertyRepository.saveAll(Arrays.asList(propertyCyberJaya, propertyKL, propertyKlang));
        String searchQuery = "cyberjaya";
        Integer area = 900;
        Integer price = 1200;
        List<Property> searchResult = propertyRepository.searchProperty(searchQuery, area, price);
        Assertions.assertFalse(searchResult.isEmpty());
        Assertions.assertEquals(searchResult.size(), 2);
    }
}
