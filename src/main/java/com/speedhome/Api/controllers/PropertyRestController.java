package com.speedhome.Api.controllers;

import com.speedhome.Api.models.Property;
import com.speedhome.Api.services.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.databind.JsonNode;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class PropertyRestController {
    @Autowired
    private PropertyService propertyService;

    @GetMapping(value = "/properties", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllProperties(){
        return ResponseEntity.status(HttpStatus.OK).body(propertyService.getAllProperties());
    }

    @GetMapping(value = "/properties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getProperty(@PathVariable long id){
        Optional<Property> property = propertyService.getPropertyById(id);
        if(!property.isPresent()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("details", "Property Not Found"));
        return ResponseEntity.status(HttpStatus.OK).body(property);
    }

    @PostMapping(value = "/properties", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveProperty(@Valid @RequestBody Property property) throws Exception {
        try {
            Property savedProperty = propertyService.addNewProperty(property);
            return ResponseEntity.status(HttpStatus.CREATED).body(savedProperty);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of("details", "Property Could Not Be Saved."));
        }
    }

    @PutMapping(value = "/properties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateProperty(@PathVariable long id, @Valid @RequestBody Property property){
        Property updatedProperty = propertyService.updateProperty(id, property);
        if(updatedProperty == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("details", "Property Not Found"));
        return ResponseEntity.status(HttpStatus.OK).body(updatedProperty);
    }

    @PostMapping(value = "/properties/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Property> searchProperty(@RequestBody JsonNode searchQuery){
        String address = searchQuery.has("address") ? searchQuery.get("address").textValue() : null;
        Integer area = searchQuery.has("area") ? searchQuery.get("area").intValue() : null;
        Integer price = searchQuery.has("price") ? searchQuery.get("price").intValue() : null;
        return propertyService.searchProperties(address, area, price);
    }

    @GetMapping(value = "properties/approve/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity approveProperty(@PathVariable long id) throws Exception{
        try {
            Property property = propertyService.approveProperty(id);
            if (property == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("details", "Property Not Found"));
            return ResponseEntity.status(HttpStatus.OK).body(Map.of("details", "Property Approved Successfully"));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of("details", "Property Could Not Be Approved."));
        }
    }
}
