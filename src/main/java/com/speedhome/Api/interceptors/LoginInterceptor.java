package com.speedhome.Api.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.servlet.handler.WebRequestHandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginInterceptor extends WebRequestHandlerInterceptorAdapter {
    public LoginInterceptor(WebRequestInterceptor requestInterceptor) {
        super(requestInterceptor);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authorizationToken = request.getHeader("Authorization");
        if(authorizationToken == null || !authorizationToken.equals("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")){
            response.resetBuffer();
            response.setStatus(response.SC_UNAUTHORIZED);
            response.addHeader("Content-Type", "application/json");
            response.getOutputStream().print("{\"details\": \"Invalid Token\"}");
            response.flushBuffer();
            return false;
        }
        return super.preHandle(request, response, handler);
    }
}
