package com.speedhome.Api.services;

import com.speedhome.Api.models.Property;
import com.speedhome.Api.repositories.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PropertyService {

    @Autowired
    private PropertyRepository propertyRepository;

    public List<Property> getAllProperties(){
        return propertyRepository.findAll();
    }

    public Optional<Property> getPropertyById(long id){return propertyRepository.findById(id);}

    public Property addNewProperty(Property property){
        return propertyRepository.save(property);
    }

    public Property updateProperty(long id, Property updatedProperty){
        Property property = propertyRepository.findById(id).orElse(null);
        if(property != null){
            updatedProperty.setId(property.getId());
            Property savedProperty = propertyRepository.save(updatedProperty);
            return savedProperty;
        }
        return null;
    }

    public List<Property> searchProperties(String address, Integer area, Integer price){
        return propertyRepository.searchProperty(address, area, price);
    }

    public Property approveProperty(long id){
        Property property = propertyRepository.findById(id).orElse(null);
        if(property != null){
            property.setApproved(true);
            propertyRepository.save(property);
            return property;
        }
        return null;
    }
}
