package com.speedhome.Api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Property {
    public Property() {
    }

    public Property(String name, String address, Integer area, Integer price, Integer totalBedroom, Integer totalBathroom, Integer totalParkingSpace, String description) {
        this.name = name;
        this.address = address;
        this.area = area;
        this.price = price;
        this.totalBedroom = totalBedroom;
        this.totalBathroom = totalBathroom;
        this.totalParkingSpace = totalParkingSpace;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private long id;

    @NotBlank
    private String name;

    @NotBlank
    private String address;

    @NotNull
    @Max(99999)
    @Min(99)
    private Integer area;

    @NotNull
    @Min(1)
    private Integer price;

    @NotNull
    @Min(1)
    private  Integer totalBedroom;

    @NotNull
    @Min(1)
    private Integer totalBathroom;

    @NotNull
    @ColumnDefault("0")
    private Integer totalParkingSpace;

    @NotBlank
    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(columnDefinition = "tinyint(1) default 0")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private boolean approved;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTotalBedroom() {
        return totalBedroom;
    }

    public void setTotalBedroom(Integer totalBedroom) {
        this.totalBedroom = totalBedroom;
    }

    public Integer getTotalBathroom() {
        return totalBathroom;
    }

    public void setTotalBathroom(Integer totalBathroom) {
        this.totalBathroom = totalBathroom;
    }

    public Integer getTotalParkingSpace() {
        return totalParkingSpace;
    }

    public void setTotalParkingSpace(Integer totalParkingSpace) {
        this.totalParkingSpace = totalParkingSpace;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
