package com.speedhome.Api.repositories;

import com.speedhome.Api.models.Property;

import java.util.List;

public interface PropertyRepositoryCustom {
    public List<Property> searchProperty(String address, Integer area, Integer price);

}
