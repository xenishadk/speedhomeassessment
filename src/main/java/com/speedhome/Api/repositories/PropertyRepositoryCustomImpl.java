package com.speedhome.Api.repositories;

import com.speedhome.Api.models.Property;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PropertyRepositoryCustomImpl implements PropertyRepositoryCustom{

    @Autowired
    private EntityManager em;

    @Override
    public List<Property> searchProperty(String address, Integer area, Integer price) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Property> cq = cb.createQuery(Property.class);
        Root<Property> property = cq.from(Property.class);
        List<Predicate> predicates =  new ArrayList<>();
        if(address != null){
            predicates.add(cb.like(property.get("address"), "%" + address + "%"));
        }
        if(area != null){
            predicates.add(cb.greaterThanOrEqualTo(property.get("area"), area));
        }
        if(price != null){
            predicates.add(cb.greaterThanOrEqualTo(property.get("price"), price));
        }
        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }
}
