# Spring Boot Assessment Project

## Tools Used:

* Spring Boot
* H2 Database (In memory)

## Limitations and Assumptions:
* This is a very quick implementation, may require refactoring but intent was to complete it quickly.
  

* Property search implemented here, is a very naive search that can only 
search the address (LIKE search), area(greater than given value)
  and price field(greater than given value).
  

* Pagination has not been applied when retrieving properties
  

* For authentication a static token has been provided
whose value is `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9`
  
## API Endpoints:
The application starts on port 8081 by default.

Endpoint | Method | Description | Test them at you own risk :)
------------- | ------------- |------------- | -------------
/properties | GET | Retrieve all of properties | `curl -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" http://localhost:8081/properties`
/properties | POST | Create new property | `curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" -H "Content-Type: application/json"  -d '{"name": "Kuala lumpur 2 Bedroom House", "address": "Kuala lumpur", "area": 1200, "price": 1500, "totalBedroom": 2, "totalBathroom": 1, "totalParkingSpace": 1, "description": "This is a hot property up for grabs. Apply Quickly or you may loose it."}' http://localhost:8081/properties`
/properties/{id} | GET | Retrieve property by id | `curl -X GET -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" http://localhost:8081/properties/1`
/properties/{id} | PUT | Update property | `curl -X PUT -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" -H "Content-Type: application/json"  -d '{"name": "Kuala lumpur 2 Bedroom House Update", "address": "cyberjaya", "area": 1200, "price": 2000, "totalBedroom": 2, "totalBathroom": 2, "totalParkingSpace": 1, "description": "This is a very hot property up for grabs. Apply Quickly or you may loose it."}' http://localhost:8081/properties/1`
/properties/approve/{id} | GET | Approve Property | `curl -X GET -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" http://localhost:8081/properties/approve/1`
/properties/search | POST | Search For Property | `curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" -H "Content-Type: application/json" -d '{"address": "cyberjaya", "area": 900, "price": 1000}' http://localhost:8081/properties/search`